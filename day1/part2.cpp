#include <iostream>
#include <fstream>
#include <vector>

std::vector<int> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector< int > inputNumbers;
	int x{ 0 };
	while (infile >> x){
		inputNumbers.push_back(x);
	}
	void close();
	return inputNumbers;
}

int main()
{
	std::vector< int > inputNumbers = inputFileToVector();	

	int firstNumber{ 0 };
	int vectorSize = inputNumbers.size() - 1;
	int sum{ 0 };
	while (firstNumber < vectorSize){
		for( int secondNumber = (firstNumber+1); secondNumber<(vectorSize-1); secondNumber++){
			for( int thirdNumber = (firstNumber+2); thirdNumber<(vectorSize-2); thirdNumber++)
			{
				sum = inputNumbers[firstNumber] + inputNumbers[secondNumber] + inputNumbers[thirdNumber];
				if(sum == 2020)
				{
					std::cout << inputNumbers[firstNumber] << '\n';
					std::cout << inputNumbers[secondNumber] << '\n';
					std::cout << inputNumbers[thirdNumber] << '\n';

					std::cout << inputNumbers[thirdNumber]*
						inputNumbers[secondNumber]*
						inputNumbers[firstNumber] << '\n';
					return 0;
				}
			}
		}
		firstNumber++;
	}
	return 0;
}
