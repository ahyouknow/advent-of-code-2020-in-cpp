#include <iostream>
#include <fstream>
#include <list>

int main()
{
	std::list<int> inputNums;

	std::ifstream infile;
	infile.open("input.txt");
	int x;
	while (infile >> x){
		inputNums.push_back(x);
	}
	void close();

	int firstNumber{ 0 };
	int secondNumber{ 0 };
	while (firstNumber+secondNumber != 2020){
		firstNumber = inputNums.front();
		inputNums.pop_front();
		for (int x : inputNums){
			secondNumber = x;
			if (firstNumber+secondNumber == 2020){
				break;
			}
		}
	}
	std::cout << firstNumber << "\n";
	std::cout << secondNumber << "\n";
	std::cout << firstNumber * secondNumber << "\n";
		

	return 0;
}
