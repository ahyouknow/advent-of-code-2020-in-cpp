#include<iostream>
#include<fstream>
#include<vector>
#include<string>

std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputNumbers;
	std::string x = "";
	while (infile >> x){
		inputNumbers.push_back(x);
	}
	void close();
	return inputNumbers;
}

std::vector<int> characterMinMax(char character, std::string range)
{
	int delimPosition = range.find_first_of(character);
	int min = std::stoi(range.substr(0, delimPosition));
	int max = std::stoi(range.substr(delimPosition+1, range.size()));
	return std::vector<int> {min, max};
}

int characterCount(std::string character, std::string inputString)
{
	int charCount{0};
	for (int x{ 0 }; x < inputString.size(); x++){
		if (inputString[x] == character[0])
			charCount++;
	}
	return charCount;
}

int main()
{
	std::vector<std::string> inputStrings = inputFileToVector();
	int vectorLength = inputStrings.size();
	std::string character{ 'x' };
	int charCount{ 0 };
	int validPasswordCount{ 0 };
	for (int position{ 0 }; position < vectorLength; position+=3){
		std::vector<int> minMax = characterMinMax('-', inputStrings[position]);
		character = inputStrings[position+1][0];
		charCount = characterCount(character, inputStrings[position+2]);
		if (minMax[0] <= charCount && minMax[1] >= charCount)
			validPasswordCount++;
	}
	std::cout << validPasswordCount << '\n';
	return 0;
}
