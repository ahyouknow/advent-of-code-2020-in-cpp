#include<iostream>
#include<fstream>
#include<vector>
#include<string>

std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputNumbers;
	std::string x = "";
	while (infile >> x){
		inputNumbers.push_back(x);
	}
	void close();
	return inputNumbers;
}

std::vector<int> getPositions(char character, std::string range)
{
	int delimPosition = range.find_first_of(character);
	int min = std::stoi(range.substr(0, delimPosition));
	int max = std::stoi(range.substr(delimPosition+1, range.size()));
	return std::vector<int> {min, max};
}

bool checkPassword(int positionOne, int positionTwo, std::string character, std::string inputString)
{
	char firstCharacter  = inputString[positionOne-1];
	char secondCharacter = inputString[positionTwo-1]; 

	bool first  = (firstCharacter == character[0]);
	bool second = (secondCharacter == character[0]);
	
	if (first != second)
		return true;
	return false;
}

int main()
{
	std::vector<std::string> inputStrings = inputFileToVector();
	int vectorLength = inputStrings.size();
	std::string character{ 'x' };
	int validPasswordCount{ 0 };
	for (int position{ 0 }; position < vectorLength; position+=3){
		std::vector<int> positions = getPositions('-', inputStrings[position]);
		character = inputStrings[position+1][0];
		if (checkPassword(positions[0], positions[1], character, inputStrings[position+2]))
			validPasswordCount++;
	}
	std::cout << validPasswordCount << '\n';
	return 0;
}
