#include<iostream>
#include<fstream>
#include<vector> 
#include<string>
#include<cmath>
std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputNumbers;
	std::string line = "";
	while ( getline (infile, line) ){
		inputNumbers.push_back(line);
	}
	void close();
	return inputNumbers;
}

int calculateSeatID(std::string seatCode)
{
	float lowerLimit = 0.0;
	float upperLimit = 127.0;
	for (int x{ 0 }; x < seatCode.size()-2; x++){
		if ( seatCode[x] == 'F' )
			upperLimit = std::floor((lowerLimit+upperLimit)/2);
		else
			lowerLimit = std::ceil((lowerLimit+upperLimit)/2);
	}
	float row = lowerLimit;

	lowerLimit = 0.0;
	upperLimit = 7.0;
	for (int x = seatCode.size()-3; x < seatCode.size(); x++){
	   if ( seatCode[x] == 'L' )  
			upperLimit = std::floor((lowerLimit+upperLimit)/2);
	   else
			lowerLimit = std::ceil((lowerLimit+upperLimit)/2);
	}
	return row*8 + upperLimit;
}


int main()
{
	std::vector<std::string> seatCodes = inputFileToVector();

	int largestSeatID{ 0 };
	int smallestSeatID{ 1023 };
	int seatID{ 0 };
	unsigned int sumOfSeatID{ 0 };
	for (int x{ 0 }; x < seatCodes.size(); x++){
		seatID = calculateSeatID(seatCodes[x]);
		sumOfSeatID+=seatID;
		if ( seatID > largestSeatID )
			largestSeatID = seatID;
		else if (seatID < smallestSeatID)
			smallestSeatID = seatID;
	}
	unsigned int theoricalSeatIDsum{ 0 };
	for (int seatID = smallestSeatID; seatID <= largestSeatID; seatID++){
		theoricalSeatIDsum+=seatID;
	}
	std::cout << theoricalSeatIDsum - sumOfSeatID;
}
