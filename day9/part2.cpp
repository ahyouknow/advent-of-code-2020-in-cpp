#include<iostream>
#include<fstream>
#include<string>
#include<vector>

std::vector<unsigned long> inputFileToVector(){
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<unsigned long> inputnumbers;
	std::string line = "";
	while ( getline (infile, line) ){
		inputnumbers.push_back(std::stoul(line));
	}
	void close();
	return inputnumbers;
}

bool checkValidity(std::vector<unsigned long> input, int position){
	const int preambleSize{ 25 };


	unsigned long number = input[position];
	unsigned long firstNumber;
	unsigned long secondNumber;
	for (int x = position - preambleSize; x < position; x++){
		firstNumber = input[x];
		for (int y = x+1; y < position; y++){
			secondNumber = input[y];
			if ( number == (firstNumber + secondNumber) ) return true;
		}
	}
	return false;
}

int main(){
	std::vector<unsigned long> input = inputFileToVector();
	const int preambleSize { 25 };

	unsigned long invalidNumber{ 0 };
	for (int line = preambleSize; line < input.size(); line++){
		invalidNumber = input[line];
		if ( !(checkValidity(input, line)) ) break;
	}

	std::cout << invalidNumber << '\n';

	int startingPosition{ 0 };
	int endingPosition{ 0 };
	unsigned long sumOfSet{ 0 };
	for (int setPosition{ 0 }; setPosition < input.size(); setPosition++){
		sumOfSet += input[setPosition];
		if ( sumOfSet == invalidNumber ){
			endingPosition = setPosition;
			break;
		}
		else if ( sumOfSet > invalidNumber ){
			sumOfSet = 0;
			startingPosition++;
			setPosition = startingPosition-1;
		}
	}

	unsigned long highestNumber{ 0 };
	unsigned long lowestNumber = invalidNumber;

	for (int position = startingPosition; position < endingPosition+1; position++){
		std::cout << "highest number:  " << highestNumber <<  '\n';
		std::cout << "lowest number:  " << lowestNumber << "\n\n";
		if ( input[position] > highestNumber ){
			highestNumber = input[position];
		}
		if ( input[position] < lowestNumber ){
			lowestNumber = input[position];
		}
	}

	std::cout << highestNumber + lowestNumber << '\n';

	return 0;
}
