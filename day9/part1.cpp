#include<iostream>
#include<fstream>
#include<string>
#include<vector>

std::vector<unsigned long> inputFileToVector(){
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<unsigned long> inputnumbers;
	std::string line = "";
	while ( getline (infile, line) ){
		inputnumbers.push_back(std::stoul(line));
	}
	void close();
	return inputnumbers;
}

bool checkValidity(std::vector<unsigned long> input, int position){
	const int preambleSize{ 25 };


	unsigned long number = input[position];
	unsigned long firstNumber;
	unsigned long secondNumber;
	for (int x = position - preambleSize; x < position; x++){
		firstNumber = input[x];
		for (int y = x+1; y < position; y++){
			secondNumber = input[y];
			if ( number == (firstNumber + secondNumber) ) return true;
		}
	}
	return false;
}

int main(){
	std::vector<unsigned long> input = inputFileToVector();
	const int preambleSize { 25 };

	unsigned long invalidNumber{ 0 };
	for (int line = preambleSize; line < input.size(); line++){
		invalidNumber = input[line];
		if ( !(checkValidity(input, line)) ) break;
	}

	std::cout << invalidNumber;
	return 0;
}
