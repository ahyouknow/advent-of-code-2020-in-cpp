#include<iostream>
#include<fstream>
#include<vector>
#include<string>
#include<bitset>
#include<cstdint>

std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputNumbers;
	std::string line = "";
	while ( getline (infile, line) ){
		inputNumbers.push_back(line);
	}
	void close();
	return inputNumbers;
}

std::vector<std::string> splitString(std::string line, const char* character)
{
	std::vector<std::string> returnValue;

	std::size_t position = line.find_first_of(character);
	while (position != std::string::npos)
	{
		returnValue.push_back(line.substr(0, position));
		line = line.substr(position+1, line.length());
		position = line.find_first_of(character);
	}
	returnValue.push_back(line);
	return returnValue;
}

std::vector<std::string> getCredentialTypes(std::string line)
{
	std::vector<std::string> splitLine = splitString(line, " ");

	std::vector<std::string> credentialTypes;
	std::vector<std::string> credentials;
	for(int x{ 0 }; x < splitLine.size(); x++){
		credentials = splitString(splitLine[x], ":");
		credentialTypes.push_back(credentials[0]);
	}
	return credentialTypes;
}

constexpr std::uint_fast32_t charTo32Bit(const char * str)
{
	std::uint_fast32_t returnValue = 0;
	std::uint_fast8_t  placeholder = 0;
	int shift{ 16 };
	for(int x{ 0 }; x < 3; x++){
	   placeholder = str[x];	   
	   returnValue |= placeholder;
	   returnValue = returnValue << shift;
	   shift-=8;
	}
	return returnValue;
}

int main()
{
	std::vector<std::string> passports = inputFileToVector();
	if (passports.back() != "") passports.push_back("");

	// Switch checks
	constexpr std::uint_fast32_t byr = charTo32Bit("byr");
	constexpr std::uint_fast32_t iyr = charTo32Bit("iyr");
	constexpr std::uint_fast32_t eyr = charTo32Bit("eyr");
	constexpr std::uint_fast32_t hgt = charTo32Bit("hgt");
	constexpr std::uint_fast32_t hcl = charTo32Bit("hcl");
	constexpr std::uint_fast32_t ecl = charTo32Bit("ecl");
	constexpr std::uint_fast32_t pid = charTo32Bit("pid");

	// Bitwise checks
	constexpr std::uint_fast8_t byrBit{ 0b0000'0001 };
	constexpr std::uint_fast8_t iyrBit{ 0b0000'0010 };
	constexpr std::uint_fast8_t eyrBit{ 0b0000'0100 };
	constexpr std::uint_fast8_t hgtBit{ 0b0000'1000 };
	constexpr std::uint_fast8_t hclBit{ 0b0001'0000 };
	constexpr std::uint_fast8_t eclBit{ 0b0010'0000 };
	constexpr std::uint_fast8_t pidBit{ 0b0100'0000 };

	constexpr std::uint_fast8_t valid{ 0b0111'1111 };

	std::uint_fast8_t passport{ 0 };
	std::vector<std::string> credentials;
	int validPassports{ 0 };
	int credentialInt{ 0 };
	
	for (int lineNum{ 0 }; lineNum < passports.size(); lineNum++)
	{
		if (passports[lineNum] == "")
		{
			if (passport == valid) validPassports++;
			passport = 0;
		}
		else
		{
			credentials = getCredentialTypes(passports[lineNum]);
			for(int x{0}; x < credentials.size(); x++){
				credentialInt = charTo32Bit(const_cast<char*>(credentials[x].c_str()));
				switch (credentialInt){
					case byr:
						passport |= byrBit;
						break;
					case iyr:
						passport |= iyrBit;
						break;
					case eyr:
						passport |= eyrBit;
						break;
					case hgt:
						passport |= hgtBit;
						break;
					case hcl:
						passport |= hclBit;
						break;
					case ecl:
						passport |= eclBit;
						break;
					case pid:
						passport |= pidBit;
						break;
				}
			}
		}
	}
	std::cout << validPassports << '\n';
	return 0;
}
