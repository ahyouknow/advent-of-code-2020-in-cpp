#include<iostream>
#include<fstream>
#include<vector>
#include<string>
#include<bitset>
#include<cstdint>

std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputNumbers;
	std::string line = "";
	while ( getline (infile, line) ){
		inputNumbers.push_back(line);
	}
	void close();
	return inputNumbers;
}

std::vector<std::string> splitString(std::string line, const char* character)
{
	std::vector<std::string> returnValue;

	std::size_t position = line.find_first_of(character);
	while (position != std::string::npos)
	{
		returnValue.push_back(line.substr(0, position));
		line = line.substr(position+1, line.length());
		position = line.find_first_of(character);
	}
	returnValue.push_back(line);
	return returnValue;
}

constexpr std::uint_fast32_t charTo32Bit(const char * str)
{
	std::uint_fast32_t returnValue = 0;
	std::uint_fast8_t  placeholder = 0;
	int shift{ 16 };
	for(int x{ 0 }; x < 3; x++){
	   placeholder = str[x];	   
	   returnValue |= placeholder;
	   returnValue = returnValue << shift;
	   shift-=8;
	}
	return returnValue;
}

bool byrCheck(std::string str)
{
	int input = std::stoi(str);
	return ( input >= 1920 && input <= 2002 );
}					
					
bool iyrCheck(std::string str)
{
	int input = std::stoi(str);
	return ( input >= 2010 && input <= 2020 );
}
					
bool eyrCheck(std::string str)
{
	int input = std::stoi(str);
	return ( input >= 2020 && input <= 2030 );
}

bool hgtCheck(std::string str)
{
	int size = str.size();
	std::string measurement = str.substr(size-2, size-1);
	int height{ 0 };
	if (measurement.compare("in") == 0){
		height = std::stoi(str.substr(0, size-2));
		return (height >= 59 && height <= 76);
	}
	else if (measurement.compare("cm") == 0){
		height = std::stoi(str.substr(0, size-2));
		return (height >= 150 && height <= 193);
	}
	else return false; 
}

bool hclCheck(std::string str)
{
	if (str[0] != '#'){
		return false;
	}
	if (str.size() != 7){
		return false;
	}
	const std::string validCharacters = "0123456789abcdef";
	for(int x{ 1 }; x < str.size(); x++){
		if (validCharacters.find_first_of(str[x]) == std::string::npos)
			return false;
	}
	return true;
}

bool eclCheck(std::string str)
{
	const std::string validColors[7] = {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"};
	for(int x{ 0 }; x < 7; x++){
		if (str.compare(validColors[x]) == 0) return true;
	}
	return false;
}

bool pidCheck(std::string str)
{
	if (str.size() == 9)
		return true;
	else 
		return false;
}

int main()
{
	std::vector<std::string> passports = inputFileToVector();
	if (passports.back() != "") passports.push_back("");
	// Switch checks
	constexpr std::uint_fast32_t byr = charTo32Bit("byr");
	constexpr std::uint_fast32_t iyr = charTo32Bit("iyr");
	constexpr std::uint_fast32_t eyr = charTo32Bit("eyr");
	constexpr std::uint_fast32_t hgt = charTo32Bit("hgt");
	constexpr std::uint_fast32_t hcl = charTo32Bit("hcl");
	constexpr std::uint_fast32_t ecl = charTo32Bit("ecl");
	constexpr std::uint_fast32_t pid = charTo32Bit("pid");

	// Bitwise checks
	constexpr std::uint_fast8_t byrBit{ 0b0000'0001 };
	constexpr std::uint_fast8_t iyrBit{ 0b0000'0010 };
	constexpr std::uint_fast8_t eyrBit{ 0b0000'0100 };
	constexpr std::uint_fast8_t hgtBit{ 0b0000'1000 };
	constexpr std::uint_fast8_t hclBit{ 0b0001'0000 };
	constexpr std::uint_fast8_t eclBit{ 0b0010'0000 };
	constexpr std::uint_fast8_t pidBit{ 0b0100'0000 };

	constexpr std::uint_fast8_t valid{ 0b0111'1111 };

	std::uint_fast8_t passport{ 0 };
	std::vector<std::string> credentials;
	std::vector<std::string> credentialValue;
	int validPassports{ 0 };
	int credentialInt{ 0 };
	std::string value;
	
	for (int lineNum{ 0 }; lineNum < passports.size(); lineNum++)
	{
		if (passports[lineNum] == "")
		{
			if (passport == valid) {
				validPassports++;
				std::cout << "valid" << "\n\n";
			}
			else std::cout << "invalid" << "\n\n";
			passport = 0;
		}
		else
		{
			credentials = splitString(passports[lineNum], " ");
			for(int x{0}; x < credentials.size(); x++){
				credentialValue = splitString(credentials[x], ":");
				credentialInt = charTo32Bit(const_cast<char*>(credentialValue[0].c_str()));
				value = credentialValue[1];
				std::cout << credentialValue[0] << ": " << credentialValue[1];
				switch (credentialInt){
					case byr:
						if (byrCheck(value)) {
							passport |= byrBit;
							std::cout << " valid";
						}
						else std::cout << " invalid";
						break;
					case iyr:
						if (iyrCheck(value)){
						   	passport |= iyrBit;
							std::cout << " valid";
						}
						else std::cout << " invalid";
						break;
					case eyr:
						if (eyrCheck(value)){
						   	passport |= eyrBit;
							std::cout << " valid";
						}
						else std::cout << " invalid";
						break;
					case hgt:
						if (hgtCheck(value)){
						   	passport |= hgtBit;
							std::cout << " valid";
						}
						else std::cout << " invalid";
						break;
					case hcl:
						if (hclCheck(value)){
						   	passport |= hclBit;
							std::cout << " valid";
						}
						else std::cout << " invalid";
						break;
					case ecl:
						if (eclCheck(value)){
						   	passport |= eclBit;
							std::cout << " valid";
						}
						else std::cout << " invalid";
						break;
					case pid:
						if (pidCheck(value)){
						   	passport |= pidBit;
							std::cout << " valid";
						}
						else std::cout << " invalid";
						break;
				}
				std::cout << '\n';
			}
		}
	}
	std::cout << validPassports << '\n';
	return 0;
}
