#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include<algorithm>

std::vector<int> inputFileToVector(){
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<int> inputnumbers;
	std::string line = "";
	while ( getline (infile, line) ){
		inputnumbers.push_back(std::stoi(line));
	}
	void close();
	return inputnumbers;
}

int main()
{
	std::vector<int> adapters = inputFileToVector();

	adapters.push_back(0); //outlet effective rating

	std::sort(adapters.begin(), adapters.end());

	adapters.push_back(adpaters.end() + 3 ); // device  effective rating

	int difference{ 0 };
	int singleDifferenceCount{ 0 };
	int tripleDifferenceCount{ 0 };
	for (int x{ 0 }; x < adapters.size() - 1; x++){
		difference = adapters[x+1] - adapters[x];
		if (difference == 1){
			singleDifferenceCount++;
		}
		else if (difference == 3){
			tripleDifferenceCount++;
		}
	}

	std::cout << "1 difference: " << singleDifferenceCount << '\n';
	std::cout << "3 difference: " << tripleDifferenceCount << '\n';
	std::cout << "mulplied:  " << singleDifferenceCount * tripleDifferenceCount << '\n';
	return 0;
}
