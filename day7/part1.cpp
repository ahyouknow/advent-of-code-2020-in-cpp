#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<vector>
#include<map>

struct Bag
{
	std::string color;

	std::vector<std::string> contains;

	std::vector<std::string> containsHowMany;
};

bool listContains(std::string str, std::vector<std::string> list)
{
	for (int x{ 0 }; x < list.size(); x++ ){
		if ( str.compare(list[x]) == 0 )
			return true;
	}
	return false;
}

class BagRulesClass
{
public:
	std::map<std::string, Bag> bagMap;
	std::vector<std::string> hasGold;
	std::vector<std::string> noGold;

	void checkBag(std::string bagColor){
		if ( listContains(bagColor, hasGold) || listContains(bagColor, noGold) ){
			return;
		}

		std::string containColor;
		bool colorHasGold{ false };
		for (int x{ 0 }; x < bagMap[bagColor].contains.size(); x++){
			containColor = bagMap[bagColor].contains[x];
			checkBag(containColor);
			if ( listContains(containColor, noGold) ){
				continue;
			}
			if ( listContains(containColor, hasGold) ){
				colorHasGold = true;
				continue;
			}
		}

		if ( colorHasGold ){
			hasGold.push_back(bagColor);
		}
		else{
			noGold.push_back(bagColor);
		}
	}
};

std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputnumbers;
	std::string line = "";
	while ( getline (infile, line) ){
		inputnumbers.push_back(line);
	}
	void close();
	return inputnumbers;
}

std::vector<std::string> splitStringBySpace(std::string str)
{
	std::string word;
	std::stringstream stream(str);
	std::vector<std::string> output;
	while (stream >> word) {
		output.push_back(word);
	}
	return output;
}

int main()
{
	std::vector<std::string> bagRules = inputFileToVector();


	Bag bag;
	std::string bagColor;
	std::string containsColor; 
	std::vector<std::string> splitLine;
	std::vector<std::string> contains;
	std::vector<std::string> containsHowMany;

	BagRulesClass bagClass;
	bool hasGold = false;
	for (int line{ 0 }; line < bagRules.size(); line++)
	{
		splitLine = splitStringBySpace(bagRules[line]);
		bagColor = splitLine[0] + " " + splitLine[1];

		hasGold = false;
		contains.clear();
		containsHowMany.clear();
		for (int eachBag{ 4 }; eachBag < splitLine.size(); eachBag+=4){
			containsHowMany.push_back( splitLine[eachBag] );

			containsColor = splitLine[eachBag+1] + " " + splitLine[eachBag+2];
			contains.push_back(containsColor);
			if ( containsColor.compare("shiny gold") == 0 ){
				bagClass.hasGold.push_back(bagColor);
				hasGold = true;
			}
			else if ( containsColor.compare("other bags.") == 0 ) {
				bagClass.noGold.push_back(bagColor);
				hasGold = true;
			}
		}
		if ( !hasGold ){
			bag = { bagColor, contains, containsHowMany };
			bagClass.bagMap[ bagColor ] = bag;
		}
	}

	std::map<std::string, Bag>::iterator iter;
	for (iter = bagClass.bagMap.begin(); iter != bagClass.bagMap.end(); iter++){
		bagClass.checkBag(iter->first);
		/*if ( iter->first.compare("bright crimson") == 0 ){
			break;
		}*/
	}
	std::cout << bagClass.hasGold.size() << '\n';
	std::cout << bagClass.noGold.size() << '\n';
	std::cout << bagRules.size() << '\n';
}
