#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<vector>
#include<map>

struct Bag
{
	std::string color;

	std::vector<std::string> contains;

	std::vector<int> containsHowMany;

};

bool listContains(std::string str, std::vector<std::string> list)
{
	for (int x{ 0 }; x < list.size(); x++ ){
		if ( str.compare(list[x]) == 0 )
			return true;
	}
	return false;
}

class BagRulesClass
{
public:
	std::map<std::string, Bag> bagMap;
	std::map<std::string, int> bagCountMap;

	void countContainedBags(std::string bagColor){
		Bag bag = bagMap[bagColor];
		if (bagCountMap[bagColor] == 0 || bagCountMap[bagColor] != -1)
			return;

		int bagCount{ 0 };
		Bag containedBag;
		for (int x{ 0 }; x < bag.contains.size(); x++ ){
			containedBag = bagMap[ bag.contains[x] ];
			countContainedBags(containedBag.color);
			bagCount += ( bag.containsHowMany[x] + bag.containsHowMany[x]*bagCountMap[containedBag.color] );
		}

		bagCountMap[bagColor] = bagCount;
	}
};

std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputnumbers;
	std::string line = "";
	while ( getline (infile, line) ){
		inputnumbers.push_back(line);
	}
	void close();
	return inputnumbers;
}

std::vector<std::string> splitStringBySpace(std::string str)
{
	std::string word;
	std::stringstream stream(str);
	std::vector<std::string> output;
	while (stream >> word) {
		output.push_back(word);
	}
	return output;
}


int main()
{
	std::vector<std::string> bagRules = inputFileToVector();


	Bag bag;
	std::string bagColor;
	std::string containsColor; 
	std::vector<std::string> splitLine;
	std::vector<std::string> contains;
	std::vector<int> containsHowMany;
	int bagCount{ -1 };

	BagRulesClass bagClass;
	for (int line{ 0 }; line < bagRules.size(); line++)
	{
		splitLine = splitStringBySpace(bagRules[line]);
		bagColor = splitLine[0] + " " + splitLine[1];

		contains.clear();
		containsHowMany.clear();
		for (int eachBag{ 4 }; eachBag < splitLine.size(); eachBag+=4){
			if ( splitLine[eachBag].compare("no") == 0 ){
				containsHowMany.push_back( 0 );
				bagClass.bagCountMap[bagColor] = 0;
			}
			else{
				containsHowMany.push_back( std::stoi(splitLine[eachBag]) );
				bagClass.bagCountMap[bagColor] = -1;
			}

			containsColor = splitLine[eachBag+1] + " " + splitLine[eachBag+2];
			contains.push_back(containsColor);
		}
		bag = { bagColor, contains, containsHowMany };
		bagClass.bagMap[ bagColor ] = bag;
	}
	bagClass.countContainedBags("shiny gold");
	bag = bagClass.bagMap["shiny gold"];
	std::cout << bagClass.bagCountMap["shiny gold"];
}
