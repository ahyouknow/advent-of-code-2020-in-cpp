#include<iostream>
#include<fstream>
#include<vector>
#include<string>

std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputNumbers;
	std::string x = "";
	while (infile >> x){
		inputNumbers.push_back(x);
	}
	void close();
	return inputNumbers;
}

int main()
{
	std::vector<std::string> forest = inputFileToVector();
	int repeatRow = forest[0].length();
	int moveRight{ 0 };
	int treeCount{ 0 };
	for ( int moveDown{ 0 }; moveDown < forest.size(); moveDown++){
		std::cout << moveDown << ' ';
		std::cout << moveRight << '\n';
		if (forest[moveDown][moveRight] == '#'){
			treeCount++;
		}
		moveRight = (moveRight+3) % repeatRow;
	}
	std::cout << treeCount;
}
