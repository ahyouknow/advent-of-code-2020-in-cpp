#include<iostream>
#include<fstream>
#include<vector>
#include<string>

std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputNumbers;
	std::string x = "";
	while (infile >> x){
		inputNumbers.push_back(x);
	}
	void close();
	return inputNumbers;
}

int treeCount(std::vector<std::string> forest, int moveRight, int moveDown)
{
	int repeatRow = forest[0].length();
	int right{ 0 };
	int treeCount{ 0 };
	for ( int down{ 0 }; down < forest.size(); down+=moveDown){
		if (forest[down][right] == '#'){
			treeCount++;
		}
		right = (right+moveRight) % repeatRow;
	}
	return treeCount;
}


int main()
{
	std::vector<std::string> forest = inputFileToVector();
	int slopes[5][2] = { {1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2} };
	unsigned long result{ 1 };
	int count{ 0 };
	for (int x{ 0 }; x < 5; x++){
		std::cout << result << '\n';
		count = treeCount(forest, slopes[x][0], slopes[x][1]);
		std::cout << count << '\n';
		result = result*count;
	}
	std::cout << result;

	return 0;
}
