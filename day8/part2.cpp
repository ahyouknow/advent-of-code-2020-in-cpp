#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<vector> 
#include<map> 

std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputnumbers;
	std::string line = "";
	while ( getline (infile, line) ){
		inputnumbers.push_back(line);
	}
	void close();
	return inputnumbers;
}

std::vector<std::string> split(std::string str)
{
	std::string word;
	std::stringstream stream(str);
	std::vector<std::string> output;
	while (stream >> word) {
		output.push_back(word);
	}
	return output;
}

bool checkCommandHistory(std::vector<int> history, int commandPosition){
	for (int x{0}; x < history.size(); x++){
		if (history[x] == commandPosition) return true;
	}
	return false;
}

long runCommands(std::vector<std::string> input){
	const std::map<std::string, int> commands = { {"acc", 0}, {"jmp", 1}, {"nop", 2 } };
	std::vector<std::string> splitLine;
	std::vector<int> history;
	long accumulator{ 0 };
	int command;
	int argument;

	history.clear();
	for (int position{ 0 }; position < input.size(); position++){
		if ( position == input.size()-1 || position < 0 ) break;
		if ( input[position].compare("jmp +0") == 0 ) return 0;
		if ( checkCommandHistory(history, position) ) {
		   	return 0;
		}
		splitLine = split(input[position]);
		command = commands.at(splitLine[0]);
		argument = std::stoi(splitLine[1]);
		history.push_back(position);

		switch (command){
			case 0: // acc
				accumulator += argument;
				break;
			case 1: // jmp
				position += argument-1;
				break;
		}

	}
	return accumulator;
}


int main(){
	std::vector<std::string> input = inputFileToVector();

	const std::map<std::string, int> commands = { {"acc", 0}, {"jmp", 1}, {"nop", 2 } };
	std::vector<std::string> splitLine;
	std::string newCommand;
	std::string oldCommand;
	int command;
	long accumulatorResult{ 0 };
	for (int position{ 0 }; position < input.size(); position++){
		splitLine = split(input[position]);
		command = commands.at(splitLine[0]);
		switch (command){
			case 0:
				continue;
			case 1:
				newCommand = "nop " + splitLine[1];
				break;
			case 2:
				newCommand = "jmp " + splitLine[1];
				break;
		}
		oldCommand = input[position];

		input[position] = newCommand;
		accumulatorResult = runCommands(input);
		if (accumulatorResult == 0){
			input[position] = oldCommand;
		}
		else{
			break;
		}
	}
	std::cout << accumulatorResult;
	return 0;
}
