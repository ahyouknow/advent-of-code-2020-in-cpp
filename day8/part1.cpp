#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<vector> 
#include<map> 

std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputnumbers;
	std::string line = "";
	while ( getline (infile, line) ){
		inputnumbers.push_back(line);
	}
	void close();
	return inputnumbers;
}

bool checkCommandHistory(std::vector<int> history, int commandPosition){
	for (int x{0}; x < history.size(); x++){
		if (history[x] == commandPosition) return true;
	}
	return false;
}

std::vector<std::string> split(std::string str)
{
	std::string word;
	std::stringstream stream(str);
	std::vector<std::string> output;
	while (stream >> word) {
		output.push_back(word);
	}
	return output;
}

int main(){
	std::vector<std::string> input = inputFileToVector();


	const std::map<std::string, int> commands = { {"acc", 0}, {"jmp", 1}, {"nop", 2 } };
	std::vector<std::string> splitLine;
	std::vector<int> history;
	int accumulator{ 0 };
	int command;
	int argument;

	for (int position{ 0 }; position < input.size(); position++){
		if ( checkCommandHistory(history, position) ) break;
		splitLine = split(input[position]);
		command = commands.at(splitLine[0]);
		argument = std::stoi(splitLine[1]);
		history.push_back(position);

		switch (command){
			case 0: // acc
				accumulator += argument;
				break;
			case 1: // jmp
				position += argument-1;
				break;
		}

	}
	std::cout << accumulator;
}
