#include<iostream>
#include<fstream> 
#include<string>
#include<vector>

std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputNumbers;
	std::string line = "";
	while ( getline (infile, line) ){
		inputNumbers.push_back(line);
	}
	void close();
	return inputNumbers;
}
unsigned int calculateYes(std::vector<std::string> groupAnswers)
{
	std::string duplicates = groupAnswers[0];
	std::string uniques = "";
	int position{ 0 };
	for (int person{ 1 }; person < groupAnswers.size(); person++){
		uniques = "";
		for (int answer{ 0 }; answer < duplicates.size(); answer++){
			if (groupAnswers[person].find_first_of(duplicates[answer]) == std::string::npos)
				uniques+=duplicates[answer];
		}
		for (int x{ 0 }; x < uniques.size(); x++){
			position = duplicates.find_first_of(uniques[x]);
			duplicates = duplicates.substr(0, position) + duplicates.substr(position+1, duplicates.size());
		}
	}
	return duplicates.size();
}


int main()
{
	std::vector<std::string> answers = inputFileToVector();
	if (answers.back() != "") answers.push_back("");
	
	unsigned int sumOfYes{ 0 };
	std::vector<std::string> groupAnswers;
	for (int x{ 0 }; x < answers.size(); x++){
		if (answers[x] != ""){
			groupAnswers.push_back(answers[x]);
		}
		else{
			sumOfYes += calculateYes(groupAnswers);
			groupAnswers.clear();
		}
	}
	std::cout << sumOfYes;
	return 0;
}
