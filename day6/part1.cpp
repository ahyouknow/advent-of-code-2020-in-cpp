#include<iostream>
#include<fstream> 
#include<string>
#include<vector>

std::vector<std::string> inputFileToVector()
{
	std::ifstream infile;
	infile.open("input.txt");
	std::vector<std::string> inputNumbers;
	std::string line = "";
	while ( getline (infile, line) ){
		inputNumbers.push_back(line);
	}
	void close();
	return inputNumbers;
}
unsigned int calculateYes(std::string groupAnswers)
{
	unsigned int yesAnswers{ 0 };
	std::string duplicates = "";
	for (int x{ 0 }; x < groupAnswers.size(); x++){
		if (duplicates.find_first_of(groupAnswers[x]) == std::string::npos){
			yesAnswers++;
			duplicates+=groupAnswers[x];
		}
	}
	return yesAnswers;
}


int main()
{
	std::vector<std::string> answers = inputFileToVector();
	if (answers.back() != "") answers.push_back("");
	
	unsigned int sumOfYes{ 0 };
	std::string groupAnswers = "";
	for (int x{ 0 }; x < answers.size(); x++){
		if (answers[x] != ""){
			groupAnswers+=answers[x];
		}
		else{
			sumOfYes+= calculateYes(groupAnswers);
			groupAnswers = "";
		}
	}
	std::cout << sumOfYes;
	return 0;
}
